import React from 'react';

class LocationForm extends React.Component {
  constructor(props) {
    super(props)
    // https://learn-2.galvanize.com/cohorts/3189/blocks/1885/content_files/build/01-stateful-components/67-form-with-react.md
    // without below line, error will be thrown "Cannot read properties of null (reading 'states')"
    // It's a timing issue. When the component first loads, there is no array of U.S. states in the component's state.
    // When it tries to render the first time, the value of 'this.state.states' is empty. We have to give it a default value.
    // this.state = {states: []};    // (this line is updated below further later in the project)
    // https://learn-2.galvanize.com/cohorts/3189/blocks/1885/content_files/build/01-stateful-components/69-form-with-react.md
    // https://reactjs.org/docs/forms.html#controlled-components
    this.state = {
      name: '',
      roomCount: '',
      city: '',
      states: []
    };
    // In JavaScript, if you have an object and you refer to one of its methods like a property, JavaScript forgets what object it came from.
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleRoomCountChange = this.handleRoomCountChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    // trple dot copies all of the properties and values from this.state into a new object. It's an easy way to make a copy of objects in JavaScript.
    const data = {...this.state};
    data.room_count = data.roomCount;
    delete data.roomCount;
    delete data.states;
    console.log(data);

    // code from how we implemented the POST code for 'Create a Location' form with HTML + Javascript (no React) in ghi/js/new-location.js
    const locationUrl = "http://localhost:8000/api/locations/"
    const fetchConfig = {
      method: "post",
      // body: json;
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json"},
    };
    const createLocationPostResponse = await fetch(locationUrl, fetchConfig);
    if (createLocationPostResponse.ok) {
      // formTag.reset();
      const newLocation = await createLocationPostResponse.json();
      console.log(newLocation);

      const cleared = {
        name: '',
        roomCount: '',
        city: '',
        state: '',
      };
      this.setState(cleared);
    }
  }

  handleNameChange(event) {
    // https://learn-2.galvanize.com/cohorts/3189/blocks/1885/content_files/build/01-stateful-components/68-form-with-react.md
    // "The event parameter is the event that occurred.
    // The target property on event is the HTML tag that caused the event.
    // So, in this case, the event.target is the input tag for the location's name.
    // Then, the value property is the value in the input."
    const nameValue = event.target.value;
    // console.log(event)
    this.setState({name: nameValue});
  }

  handleRoomCountChange(event) {
    const roomCountValue = event.target.value;
    this.setState({roomCount: roomCountValue});
  }

  handleCityChange(event) {
    const cityValue = event.target.value;
    // console.log(event)
    this.setState({city: cityValue});
  }

  handleStateChange(event) {
    const stateValue = event.target.value;
    this.setState({state: stateValue});
  }

  async componentDidMount() {
    const stateListUrl = "http://localhost:8000/api/states/";
    const stateListResponse = await fetch(stateListUrl);

    if (stateListResponse.ok) {
      const stateData = await stateListResponse.json();

      this.setState({states: stateData.states});

      // code from how we implemented the 'Create a Location' form with HTML + Javascript (no React) in ghi/js/new-location.js
      // const selectTag = document.getElementById('state');
      // // For each state in the states property of the data
      // for (let state of stateData["states"]) {
      //   // Create an 'option' element
      //   const stateOption = document.createElement('option');
      //   // Set the '.value' property of the option element to the state's abbreviation
      //   stateOption.value = state.abbreviation;
      //   // Set the '.innerHTML' property of the option element to the state's name
      //   stateOption.innerHTML = state.name;
      //   // Append the option element as a child of the select tag
      //   selectTag.appendChild(stateOption);
      // }
    }
  }
  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                {/* https://reactjs.org/docs/forms.html#controlled-components */}
                {/* the value={this.state.name} is needed so that the value of the form on the page (UI) shows what's stored in the state */}
                {/* so for example, when the form is submitted by user and the handleSubmit() method is called: */}
                {/* the state changes to empty strings. Without the value={this.state.name}, the UI will still show what the user typed */}
                {/* Wtih the value={this.state.name}, now the UI will also show what is stored in the state */}
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleRoomCountChange} value={this.state.roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCityChange} value={this.state.city} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleStateChange} value={this.state.state} required name="state" id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {this.state.states.map(state => {
                    return (
                      <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LocationForm;