import React from 'react';

class PresentationForm extends React.Component {

  constructor () {
    super()
    this.state = {
      presenterName: '',
      presenterEmail: '',
      companyName: '',
      title: '',
      synopsis: '',
      conference: '',
      conferences: []
    };
    this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
    this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
    this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
    this.handleConferenceChange = this.handleConferenceChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit (event) {
    event.preventDefault();
    let data = {...this.state};

    data.presenter_name = data.presenterName;
    delete data.presenterName;

    data.presenter_email = data.presenterEmail;
    delete data.presenterEmail;

    data.company_name = data.companyName;
    delete data.companyName;

    delete data.conference;
    delete data.conferences;

    console.log(data);

    const presentationUrl = `http://localhost:8000/api/conferences/${this.state.conference}/presentations/`
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {"Content-type": "application/json"},
      }
      const createPresentationPostResponse = await fetch(presentationUrl, fetchConfig);
      if (createPresentationPostResponse.ok) {
        const newPresentation = await createPresentationPostResponse.json();
        console.log(newPresentation);

        const cleared = {
          presenterName: '',
          presenterEmail: '',
          companyName: '',
          title: '',
          synopsis: '',
          conference: '',
        };
        this.setState(cleared);

      }

  }

  handlePresenterNameChange(event) {
    const presenterNameValue = event.target.value
    this.setState({presenterName: presenterNameValue})
  }

  handlePresenterEmailChange (event) {
    const presenterEmailValue = event.target.value
    this.setState({presenterEmail: presenterEmailValue})
  }

  handleCompanyNameChange (event) {
    const companyNameValue = event.target.value
    this.setState({companyName: companyNameValue})
  }

  handleTitleChange (event) {
    const titleValue = event.target.value
    this.setState({title: titleValue})
  }

  handleSynopsisChange (event) {
    const synopsisValue = event.target.value
    this.setState({synopsis: synopsisValue})
  }

  handleConferenceChange (event) {
    const conferenceValue = event.target.value
    this.setState({conference: conferenceValue})
  }

  async componentDidMount() {
    const conferenceListUrl = "http://localhost:8000/api/conferences/";
    const conferenceListResponse = await fetch(conferenceListUrl);

    if(conferenceListResponse.ok) {
      const conferenceData = await conferenceListResponse.json();
      this.setState({conferences: conferenceData.conferences})
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterNameChange} value={this.state.presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterEmailChange} value={this.state.presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyNameChange} value={this.state.companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} placeholder="Conference synopsis" required name="synopsis" id="synopsis" className="form-control" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleConferenceChange} value={this.state.conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences.map (conference => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;