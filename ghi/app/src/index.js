import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// https://learn-2.galvanize.com/cohorts/3189/blocks/1883/content_files/build/05-react/67-attendees-list.md
async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  // console.log(response);

  if(response.ok) {
    const data = await response.json();
    // console.log(data);
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadAttendees();