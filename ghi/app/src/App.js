import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  // console.log("in App.js")
  return (
    // <React.Fragment>
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
        </Routes>
      {/* </div> */}
    </BrowserRouter>
    // </React.Fragment>
    );
  }


export default App;

// originally had this code inside of function App > return > React.Fragment > div
// while building each form individually, to see how they reach rendered on the browser
// <LocationForm />
// <ConferenceForm />
// <AttendConferenceForm />
// <AttendeesList attendees={props.attendees} />