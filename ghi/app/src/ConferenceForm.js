import React from 'react';

class ConferenceForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      starts: '',
      ends: '',
      description: '',
      maxPresentations: '',
      maxAttendees: '',
      location: '',
      locations: []
    }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
    this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.max_attendees = data.maxAttendees;
    delete data.maxAttendees;
    data.max_presentations = data.maxPresentations;
    delete data.maxPresentations;
    delete data.locations;
    // console.log(data);

    const conferenceUrl = "http://localhost:8000/api/conferences/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json"},
    };
    const createConferencePostResponse = await fetch(conferenceUrl, fetchConfig);
    if (createConferencePostResponse.ok) {
      const newConference = await createConferencePostResponse.json();
      console.log(newConference);

      const cleared = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxPresentations: '',
        maxAttendees: '',
        location: '',
      }
      this.setState(cleared);
    }
  }

  handleNameChange(event) {
    const nameValue = event.target.value;
    this.setState({name: nameValue})
  }

  handleStartDateChange(event) {
    const startDateValue = event.target.value;
    this.setState({starts: startDateValue})
  }

  handleEndDateChange(event) {
    const endDateValue = event.target.value;
    this.setState({ends: endDateValue})
  }

  handleDescriptionChange(event) {
    const descriptionValue = event.target.value;
    this.setState({description: descriptionValue})
  }

  handleMaxPresentationsChange(event) {
    const maxPresentationsValue = event.target.value;
    this.setState({maxPresentations: maxPresentationsValue})
  }

  handleMaxAttendeesChange(event) {
    const maxAttendeesValue = event.target.value;
    this.setState({maxAttendees: maxAttendeesValue})
  }

  handleLocationChange(event) {
    const locationValue = event.target.value;
    this.setState({location: locationValue})
  }

  async componentDidMount() {
    const locationListUrl = "http://localhost:8000/api/locations/";
    const locationListResponse = await fetch(locationListUrl);

    if(locationListResponse.ok) {
      const locationData = await locationListResponse.json();
      this.setState({locations: locationData.locations});
      // console.log(locationData["locations"]);
      // const selectTag = document.getElementById('location');
      // for (const location of locationData["locations"]) {
      //   // console.log(location);
      //   const locationOption = document.createElement('option');
      //   const locationId = location.id
      //   const locationName = location.name
      //   locationOption.value = locationId;
      //   locationOption.innerHTML = locationName;
      //   selectTag.appendChild(locationOption);
      }
    }

  render() {
    return (

      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartDateChange} value={this.state.starts} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndDateChange} value={this.state.ends} placeholder="mm/dd/yyyy" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={this.handleDescriptionChange} value={this.state.description} placeholder="Conference description" required name="description" id="description" className="form-control" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;


