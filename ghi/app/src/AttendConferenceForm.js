import React from 'react';

class AttendConferenceForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      conference: '',
      fullName: '',
      email: '',
      conferences: [],
      formSubmittedSuccessfully: false
    }
    this.handleConferenceChange = this.handleConferenceChange.bind(this);
    this.handleFullNameChange = this.handleFullNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.name = data.fullName;
    delete data.fullName;
    delete data.conferences;
    delete data.formSubmittedSuccessfully;
    // console.log(data);

    const attendeeUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {"Content-type": "application/json"},
    };
    const createAttendeePostResponse = await fetch(attendeeUrl, fetchConfig);
    if(createAttendeePostResponse.ok) {
      const newAttendee = await createAttendeePostResponse.json();
      console.log(newAttendee);

      const cleared = {
        conference: '',
        fullName: '',
        email: '',
        formSubmittedSuccessfully: true,
      }
      this.setState(cleared);

    }
  }

  handleConferenceChange(event) {
    const conferenceValue = event.target.value;
    this.setState({conference: conferenceValue})
  }

  handleFullNameChange(event) {
    const fullNameValue = event.target.value;
    this.setState({fullName: fullNameValue})
  }

  handleEmailChange(event) {
    const emailValue = event.target.value;
    this.setState({email: emailValue})
  }

  async componentDidMount() {
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const conferenceResponse = await fetch(conferenceUrl);
    if(conferenceResponse.ok) {
      const conferenceData = await conferenceResponse.json();
      this.setState({conferences: conferenceData.conferences})
    }
  }

  render() {
    // https://learn-2.galvanize.com/cohorts/3189/blocks/1885/content_files/build/01-stateful-components/70-form-with-react.md
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (this.state.conferences.length > 0) {
      spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
      dropdownClasses = 'form-select';
    }
    let formSubmittedSuccessfullyClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (this.state.formSubmittedSuccessfully === true) {
      // show the success message div
      formSubmittedSuccessfullyClasses = 'alert alert-success mb-0';
      // hide the form to sign up/create an attendee
      formClasses = 'd-none';
    }

    return (
      <div className="my-5 container">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className={formClasses} id="create-attendee-form">
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                  </p>
                  <div className={spinnerClasses} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleConferenceChange} value={this.state.conference} name="conference" id="conference" className={dropdownClasses} required>
                      <option value="">Choose a conference</option>
                      {this.state.conferences.map(conference => {
                        return (
                          <option key={conference.href} value={conference.href}>
                            {conference.name}
                          </option>
                        )
                      })}
                    </select>
                  </div>
                  <p className="mb-3">
                    Now, tell us about yourself.
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleFullNameChange} value={this.state.fullName} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleEmailChange} value={this.state.email} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div className={formSubmittedSuccessfullyClasses} id="success-message">
                  Congratulations! You're all signed up!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AttendConferenceForm;