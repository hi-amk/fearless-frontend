function createCard(name, locationName, description, pictureUrl, startDate, endDate) {
  return `
    <div class="col-sm-3">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">${startDate} - ${endDate}</small>
        </div>
      </div>
    </div>
  `;
}

function generateError() {
  return `
  <div class="alert alert-danger" role="alert">
    Something went wrong!
  </div>
  `
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      throw new Error('Response not ok');
    } else {
      const data = await response.json();

      // below is original hard-coded code for 1 conference card
      // const conference = data.conferences[0];
      // const nameTag = document.querySelector('.card-title');
      // nameTag.innerHTML = conference.name;

      // const detailUrl = `http://localhost:8000${conference.href}`;
      // const detailResponse = await fetch(detailUrl);
      // if (detailResponse.ok) {
      //   const details = await detailResponse.json();
      //   console.log(details);
      //   const conferenceDescription = details["conference"]["description"];

      //   const descriptionTag = document.querySelector('.card-text');
      //   descriptionTag.innerHTML = conferenceDescription;

      //   const imageTag = document.querySelector('.card-img-top');
      //   imageTag.src = details["conference"]["location"]["picture_url"];
      //   // imageTag.src = details.conference.location.picture_url;

      // for (let conference of data.conferences) {
      //   const detailUrl = `http://localhost:8000${conference.href}`;
      //   const detailResponse = await fetch(detailUrl);
      //   if (detailResponse.ok) {
      //     const details = await detailResponse.json();
      //     const title = details.conference.title;
      //     const description = details.conference.description;
      //     const pictureUrl = details.conference.location.picture_url;
      //     const html = createCard(title, description, pictureUrl);
      //     console.log(html);
      //   }
      // }
      // ATTEMPT AT COLUMNS
      // for (let index = 0; index < data.conferences.length; index++) {
      // // for (let conference of data.conferences) {
      //   const conference = data.conferences[index];
      //   const detailUrl = `http://localhost:8000${conference.href}`;
      //   const detailResponse = await fetch(detailUrl);
      //   if (detailResponse.ok) {
      //     const details = await detailResponse.json();
      //     const name = details.conference.name;
      //     const description = details.conference.description;
      //     const pictureUrl = details.conference.location.picture_url;
      //     const html = createCard(name, description, pictureUrl);
      //     // console.log(html);
      //     if (index % 3 === 0) {
      //       const column = document.querySelector('.col-0');
      //       column.innerHTML += html;
      //     } else if (index % 3 === 1) {
      //       const column = document.querySelector('.col-1');
      //       column.innerHTML += html;
      //     } else if (index % 3 === 2) {
      //       const column = document.querySelector('.col-2');
      //       column.innerHTML += html;
      //     }
      //   }
      // }
      // for (let index = 0; index < data.conferences.length; index++) {
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDateStr = details.conference.starts;
          // https://livecodestream.dev/post/date-manipulation-in-javascript-a-complete-guide/
          const startDateJSDateObj = new Date(startDateStr);
          const startDate = startDateJSDateObj.toLocaleDateString();
          // console.log(startDate, typeof(startDate));
          const endDateStr = details.conference.ends;
          const endDateJSDateObj = new Date(endDateStr);
          const endDate = endDateJSDateObj.toLocaleDateString();

          const locationName = details.conference.location.name;

          const html = createCard(name, locationName, description, pictureUrl, startDate, endDate);
          // console.log(html);
          const column = document.querySelector('.row');
          column.innerHTML += html;

        }
      }

    }

  } catch (error) {
    // console.error('error', error);
    const errorHtml = generateError();
    const rowArea = document.querySelector('.row');
    rowArea.innerHTML += errorHtml;
    // Figure out what to do if an error is raised
  }
});