// console.log("I'm alive!")

window.addEventListener('DOMContentLoaded', async () => {
  // console.log("page loaded");
  const conferenceListUrl = "http://localhost:8000/api/conferences/";
  const conferenceListResponse = await fetch(conferenceListUrl);

  if(conferenceListResponse.ok) {
    const conferenceData = await conferenceListResponse.json();
    // console.log(conferenceData["conferences"]);
    const selectTag = document.getElementById('conference');

    for (const conference of conferenceData["conferences"]) {
      // console.log(conference);
      const conferenceOption = document.createElement('option');

      const conferenceId = conference.id
      const conferenceName = conference.name

      conferenceOption.value = conferenceId;
      conferenceOption.innerHTML = conferenceName;

      selectTag.appendChild(conferenceOption);

    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData.entries()));

      console.log(json);

      // https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement#get_information_about_the_selected_option
      // console.log(selectTag.options[selectTag.selectedIndex].value)
      const selectedConferenceId = selectTag.options[selectTag.selectedIndex].value
      // console.log(selectedConferenceId)

      const presentationUrl = `http://localhost:8000/api/conferences/${selectedConferenceId}/presentations/`
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {"Content-type": "application/json"},
      }
      const createPresentationPostResponse = await fetch(presentationUrl, fetchConfig);
      if (createPresentationPostResponse.ok) {
        formTag.reset();
        const newPresentation = await createPresentationPostResponse.json();
        console.log(newPresentation);
      }
    });

  } else {
    throw new Error('Conference List Response not ok')
  }
});