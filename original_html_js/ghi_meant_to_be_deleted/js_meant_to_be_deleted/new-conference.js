// console.log("I'm alive!")

window.addEventListener('DOMContentLoaded', async () => {
  // console.log("page loaded!")
  const locationListUrl = "http://localhost:8000/api/locations/";
  const locationListResponse = await fetch(locationListUrl);

  if(locationListResponse.ok) {
    const locationData = await locationListResponse.json();
    // console.log(locationData["locations"]);
    const selectTag = document.getElementById('location');

    for (const location of locationData["locations"]) {
      // console.log(location);
      const locationOption = document.createElement('option');

      const locationId = location.id
      const locationName = location.name

      locationOption.value = locationId;
      locationOption.innerHTML = locationName;

      selectTag.appendChild(locationOption);
      // console.log(locationId + " - " + locationName);

    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData.entries()));
      // the above and below code return exactly the same thing.
      // const json = JSON.stringify(Object.fromEntries(formData));
      // console.log(json);

      const conferenceUrl = "http://localhost:8000/api/conferences/"
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {"Content-type": "application/json"},
      };
      const createConferencePostResponse = await fetch(conferenceUrl, fetchConfig);
      if (createConferencePostResponse.ok) {
        formTag.reset();
        const newConference = await createConferencePostResponse.json();
        console.log(newConference);
      }
    });

  } else {
    throw new Error('Location List Response not ok')
  }
});