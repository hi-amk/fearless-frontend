window.addEventListener('DOMContentLoaded', () => {
  const form = document.getElementById('login-form');
  form.addEventListener('submit', async event => {
    event.preventDefault();

    // const data = Object.fromEntries(new FormData(form))
    const fetchOptions = {
      method: 'post',
      body: new FormData(form),
      // https://learn-2.galvanize.com/cohorts/3189/blocks/1883/content_files/build/04-auth-and-apis/68-sign-up-sign-in.md
      // if you're using JavaScript to hit that endpoint, you need to use just normal form data. The djwto does not do JSON POST requests.
      // headers: {
        //   'Content-Type': 'application/json',
        // }
      credentials: 'include',
    };
    const url = 'http://localhost:8000/login/';
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      window.location.href = '/';
    } else {
      console.error(response);
    }
  });
});