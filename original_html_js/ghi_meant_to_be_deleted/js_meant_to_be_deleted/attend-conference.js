window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const conferenceUrl = 'http://localhost:8000/api/conferences/';
  const conferenceResponse = await fetch(conferenceUrl);
  if(conferenceResponse.ok) {
    const conferenceData = await conferenceResponse.json();

    for (let conference of conferenceData.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    // Here, add the 'd-none' class to the loading icon
    const loadingSpinnerDivTag = document.getElementById('loading-conference-spinner');
    loadingSpinnerDivTag.classList.add("d-none");

    // Here, remove the 'd-none' class from the select tag
    const conferenceDropdownSelectTag = document.getElementById('conference');
    conferenceDropdownSelectTag.classList.remove("d-none");

    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData.entries()));
      // console.log(json);

      const attendeeUrl = "http://localhost:8001/api/attendees/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {"Content-type": "application/json"},
      };
      const createAttendeePostResponse = await fetch(attendeeUrl, fetchConfig);
      if(createAttendeePostResponse.ok) {
        const newAttendee = await createAttendeePostResponse.json();
        console.log(newAttendee);

        const successAlertDivTag = document.getElementById('success-message');
        successAlertDivTag.classList.remove("d-none");

        formTag.classList.add("d-none");

      }
    });

  } else {
    throw new Error('Conference List Response not ok')
  }
});