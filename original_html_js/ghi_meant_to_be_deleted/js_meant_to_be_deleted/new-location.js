// console.log("new location working!")

// call that RESTful API for listing states
// get the data back
// loop through it
// for each state in it, create an option element that has value of abb and test of name


// add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
  // console.log("Page loaded!")
  const stateListUrl = "http://localhost:8000/api/states/";
  const stateListResponse = await fetch(stateListUrl);

  if (stateListResponse.ok) {
    const stateData = await stateListResponse.json();
    // console.log(stateData["states"]);

    // Get the select tag element by its id 'state'
    const selectTag = document.getElementById('state');

    // For each state in the states property of the data
    for (const state of stateData["states"]) {
      // Create an 'option' element
      const stateOption = document.createElement('option');

      const stateAbbr = state.abbreviation;
      const stateName = state.name;

      // console.log(stateAbbr)
      // console.log(stateName)

      // Set the '.value' property of the option element to the state's abbreviation
      stateOption.value = stateAbbr;
      // Set the '.innerHTML' property of the option element to the state's name
      stateOption.innerHTML = stateName;
      // Append the option element as a child of the select tag
      selectTag.appendChild(stateOption);
      // console.log(stateAbbr + " - " + stateName);
    }


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      // console.log('prints when submit button in form is clicked/(enter)');

      const formData = new FormData(formTag);
      // console.log(formData);

      // Store the JSON data from the form, turn it into a string by JSON.stringify()
      // json is a string
      // FormData object expects name attributes on the form inputs
      // https://learn-2.galvanize.com/cohorts/3189/blocks/1883/content_files/build/03-front-end-state/67-front-end-state.md
      const json = JSON.stringify(Object.fromEntries(formData.entries()));
      // https://developer.mozilla.org/en-US/docs/Web/API/FormData/entries
      // console.log(formData.entries()); // returns Iterator {} object
      // console.log(Object.fromEntries(formData.entries())); // returns object with <key form-field-name: value user-input>
      // console.log(typeof(Object.fromEntries(formData.entries()))) // object
      // console.log(json); // returns string with <key form-field-name: value user-input>
      // console.log(typeof(json)); // string

      // the above and below code return exactly the same thing.
      // const json = JSON.stringify(Object.fromEntries(formData));

      const locationUrl = "http://localhost:8000/api/locations/"
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {"Content-type": "application/json"},
      };
      const createLocationPostResponse = await fetch(locationUrl, fetchConfig);
      if (createLocationPostResponse.ok) {
        formTag.reset();
        const newLocation = await createLocationPostResponse.json();
        console.log(newLocation);
      }

    });


  } else {
    throw new Error('State List Response not ok')
  }
});